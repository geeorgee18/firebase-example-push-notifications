import firebase from "firebase/app";
import "firebase/messaging";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyD_r89mgtlXPfGf4m8c-qmEEQQHVGXne4U",
  authDomain: "fire-app-practicas.firebaseapp.com",
  projectId: "fire-app-practicas",
  storageBucket: "fire-app-practicas.appspot.com",
  messagingSenderId: "867179044804",
  appId: "1:867179044804:web:4284afd5b949fc82918e91",
  measurementId: "G-LVQWFTMMFB"
};

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();

const { REACT_APP_VAPID_KEY } = process.env;
const publicKey = REACT_APP_VAPID_KEY;

export const getToken = async (setTokenFound) => {
  let currentToken = "";

  try {
    currentToken = await messaging.getToken({ vapidKey: publicKey });
    if (currentToken) {
      setTokenFound(true);
    } else {
      setTokenFound(false);
    }
  } catch (error) {
    console.log("An error occurred while retrieving token. ", error);
  }

  return currentToken;
};

export const onMessageListener = () =>
  new Promise((resolve) => {
    messaging.onMessage((payload) => {
      resolve(payload);
    });
  });
